<?php if ($this->tProjet) : ?>
	<section class="content">
		<h1 class="text-center" style="margin-top: 0px;">Liste de projets</h1>
		<ul class="projets">

			<?php foreach ($this->tProjet as $sProjet) : ?>
				<li <?php if (_root::getParam('id') == $sProjet) : ?>class="selectionne" <?php endif; ?>>
					<span><?php echo $sProjet ?></span>

					<a href="<?php
								echo _root::getLink(
									'builder::edit',
									array('id' => $sProjet)
								)
								?>"><?php echo tr('menuNavProject_link_edit') ?></a>

					<a href="<?php
								echo _root::getLink(
									'code::index',
									array('project' => $sProjet)
								)
								?>"><?php echo tr('menuNavProject_link_explore') ?></a>


					<a target="_blank" href="<?php echo _root::getConfigVar('path.generation') ?><?php echo $sProjet ?>"><?php echo tr('menuNavProject_link_gotoSite') ?></a>
				</li>
			<?php endforeach; ?>
		</ul>

		<?php
		if (_root::getParam(':nav') == 'builder::edit' && _root::getParam('id')) : ?>
			<ul>
				<h2 class="text-center">
					<li style="list-style:none;"><?php echo _root::getParam('id') ?></li>
				</h2>
			</ul>
		<?php endif; ?>
	</section>
<?php else : ?>
	<h1>Pas de projets créés</h1>
<?php endif; ?>