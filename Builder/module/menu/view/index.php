<header class="main-header">
	<!-- Logo -->
	<a target="_blank" href="http://mkframework.com" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>M</b>kf</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>M</b>KF</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
          		<li class="dropdown messages-menu">
            		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
		              <i class="fa fa-trademark"></i>
		            </a>
            		<ul class="dropdown-menu">
              			<li class="header">Builder <strong><?php echo $this->versionBuilder;?></strong></li>
              			<li class="header">Framework <strong><?php echo $this->versionLib; ?></strong></li>
          			</ul>
          		</li>

				<!-- Control Sidebar Toggle Button -->
				  <!--<li>
				    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
				  </li>-->
			</ul>
		</div>
	</nav>
</header>