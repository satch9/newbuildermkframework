<?php
/*
This file is part of Mkframework.

Mkframework is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License.

Mkframework is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mkframework.  If not, see <http://www.gnu.org/licenses/>.

*/
class module_menuAsideLeft extends abstract_module
{

	public function _index()
	{
		$tProjet = $this->getList();

		$oTpl = new _tpl('menuAsideLeft::index');
		$oTpl->tProjet = $tProjet;

		return $oTpl;
	}

	private function getList()
	{
		$oProjetModel = new model_mkfbuilderprojet;
		$tProjet = $oProjetModel->findAll();

		sort($tProjet); //tri par ordre alphabetique

		/* $oTpl = new _tpl('menuAsideLeft::list'); */
		/* $oTpl->tProjet = $tProjet; */

		return $tProjet;
	}
}
