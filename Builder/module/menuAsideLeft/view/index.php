<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="css/images/logo.jpg" class="img-circle" alt="Logo Image">
      </div>
      <div class="pull-left info">
        <p><span class="logo-lg"><b>MK</b>framework</span></p>

        <?php if (_root::getConfigVar('language.default') == 'fr') : ?>
          <a class="default" href="<?php echo _root::getLink('builder::lang', array('switch' => 'fr')) ?>"><i class="fa fa-circle text-success"></i>FR</a> |
          <a href="<?php echo _root::getLink('builder::lang', array('switch' => 'en')) ?>"><i class="fa fa-circle text-danger"></i>EN</a>
        <?php else : ?>
          <a class="default" href="<?php echo _root::getLink('builder::lang', array('switch' => 'fr')) ?>"><i class="fa fa-circle text-danger"></i>FR</a> |
          <a class="default" href="<?php echo _root::getLink('builder::lang', array('switch' => 'en')) ?>"><i class="fa fa-circle text-success"></i>EN</a>
        <?php endif; ?>

      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat">
            <i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">VUE GLOBALE</li>
      <li class="treeview menu-open">
        <a href="index.php?:nav=builder::index"><i class="fa fa-dashboard"></i> <span>Tableau de bord</span></a>
      </li>
      <li class="header">DOCUMENTATIONS</li>
      <li><a target="_blank" href="http://mkframework.com/start.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
      <li class="header">GESTION DES PROJETS</li>
      <li>
        <a href="index.php?:nav=builder::new">
          <i class="fa fa-th"></i> <span>Projet</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green">Nouveau</small>
          </span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Administration</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview">
            <a href=""><i class="fa fa-circle-o"></i> Liste des projets
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">

              <?php foreach ($this->tProjet as $sProjet) : ?>
                <li <?php if (_root::getParam('id') == $sProjet) : ?>class="selectionne" <?php endif; ?>>
                  <span style="color:white;"><?php echo $sProjet ?></span>

                  <a href="<?php
                            echo _root::getLink(
                              'builder::edit',
                              array('id' => $sProjet)
                            )
                            ?>"><?php echo tr('menuNavProject_link_edit') ?></a>

                  <a href="<?php
                            echo _root::getLink(
                              'code::index',
                              array('project' => $sProjet)
                            )
                            ?>"><?php echo tr('menuNavProject_link_explore') ?></a>


                  <a target="_blank" href="<?php echo _root::getConfigVar('path.generation') ?><?php echo $sProjet ?>"><?php echo tr('menuNavProject_link_gotoSite') ?></a>
                </li>
              <?php endforeach; ?>

              <!-- <li><a href="index.php?:nav=builder::list"><i class="fa fa-circle-o"></i> Consulter</a></li> -->
            </ul>
          </li>
        </ul>
      </li>
      <li class="header">MARKET</li>
      <li><a href="index.php?:nav=builder::marketBuilder&action=updates"><i class="fa fa-circle-o"></i> Vérifier les mises à jour</a></li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-circle-o"></i> <span>Ajouter des extensions</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="index.php?:nav=builder::marketBuilder&amp;action=install&amp;market=all_list_1"><i class="fa fa-circle-o"></i> Pour toutes les <br>applications</a></li>
          <li><a href="index.php?:nav=builder::marketBuilder&amp;action=install&amp;market=normal_list_1"><i class="fa fa-circle-o"></i> Compatibles Applications <br>"Normales"</a></li>
          <li><a href="index.php?:nav=builder::marketBuilder&amp;action=install&amp;market=bootstrap_list_1"><i class="fa fa-circle-o"></i> Compatibles Applications <br>"Bootstrap"</a></li>
          <li><a href="index.php?:nav=builder::marketBuilder&amp;action=install&amp;market=builder_list_1"><i class="fa fa-circle-o"></i> Pour le Builder</a></li>
        </ul>
      </li>
  </section>
  <!-- /.sidebar -->
</aside>